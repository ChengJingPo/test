//
//  AppDelegate.h
//  firstapp
//
//  Created by cjq on 2018/10/23.
//  Copyright © 2018年 cjq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

